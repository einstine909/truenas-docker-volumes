package main

import (
	"log"
	"os"
	"strconv"

	truenas "github.com/dariusbakunas/truenas-go-sdk"
	"github.com/docker/go-plugins-helpers/volume"
)

func main() {
	truenas_host := os.Getenv("TRUENAS_DOCKER_VOLUMES_HOST")                // TrueNAS Host
	api_key := os.Getenv("TRUENAS_DOCKER_VOLUMES_API_KEY")                  // TrueNAS API KEY
	parent_dataset_id := os.Getenv("TRUENAS_DOCKER_VOLUMES_PARENT_DATASET") // ID of the dataset that will be the parent of all volume datasets
	gid_str := os.Getenv("TRUENAS_DOCKER_VOLUMES_SOCKET_GID")               // Plugin Socket GID

	if truenas_host == "" {
		log.Fatalln("Host not Specified")
	}

	if api_key == "" {
		log.Fatalln("API Key not Specified")
	}

	if parent_dataset_id == "" {
		log.Fatalln("Parent Dataset not Specified")
	}

	if gid_str == "" {
		log.Fatalln("Socket GID not Specified")
	}

	gid, err := strconv.Atoi(gid_str)
	if err != nil {
		log.Fatalln("Socket GID invalid")
	}

	server_config := truenas.ServerConfiguration{URL: "http://" + truenas_host + "/api/v2.0"} // Create Server Config for the TrueNAS OpenAPI Spec
	server_configs := [1]truenas.ServerConfiguration{server_config}                           // Shove that into a list of one servers (there can only be one)

	var headers = map[string]string{
		"Authorization": "Bearer " + api_key, // Setup Auth Header
	}

	configuration := truenas.Configuration{Host: truenas_host, DefaultHeader: headers, Debug: false, Servers: server_configs[:]} // Init OpenAPI Spec Config

	api_client := truenas.NewAPIClient(&configuration) // Init API Client

	volume_driver := NewVolumeDriver(api_client, parent_dataset_id)
	handler := volume.NewHandler(volume_driver)
	err = handler.ServeUnix("truenas-docker-volumes", gid)

	log.Fatalln(err)
}
