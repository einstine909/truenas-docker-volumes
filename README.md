# truenas-docker-volumes

truenas-docker-volumes is a Docker plugin to create volumes stored in ZFS datasets on TrueNAS, and mount them over NFS for use with containers.

## Installation

1. Make sure the package `nfs-common` is installed (Debian varients), or that your system can mount NFSv4 shares.
2. Save the latest binary from the [Releases](https://gitlab.com/einstine909/truenas-docker-volumes/-/releases) page to a location on your machine.
3. Set up truenas-docker-volumes to run on startup. Below is a Systemd example:
```
# /etc/systemd/system/truenas-docker-volumes.service
[Unit]
Description=TrueNAS Docker Volumes
Before=docker.service
After=network.target truenas-docker-volumes.socket
Requires=truenas-docker-volumes.socket docker.service

[Service]
Environment="TRUENAS_DOCKER_VOLUMES_HOST=truenas.lan"
Environment="TRUENAS_DOCKER_VOLUMES_API_KEY=1-BlahBlahBlahBlahBlahBlahBlah"
Environment="TRUENAS_DOCKER_VOLUMES_PARENT_DATASET=tank/docker-volumes"
Environment="TRUENAS_DOCKER_VOLUMES_SOCKET_GID=134"

ExecStart=/location/of/your/truenas-docker-volumes

[Install]
WantedBy=multi-user.target
```

```
# /etc/systemd/system/truenas-docker-volumes.socket
[Unit]
Description=TrueNAS Docker Volumes Socket

[Socket]
ListenStream=/run/docker/plugins/truenas-docker-volumes.sock

[Install]
WantedBy=sockets.target
```

### Environment Variables
truenas-docker-volumes is configured by these Environment Variables:

- `TRUENAS_DOCKER_VOLUMES_HOST` The TrueNAS host that truenas-docker-volumes will connect to
- `TRUENAS_DOCKER_VOLUMES_API_KEY` The API key that you generated in the TrueNAS admin panel
- `TRUENAS_DOCKER_VOLUMES_PARENT_DATASET` The dataset that will house the Docker volume datasets
- `TRUENAS_DOCKER_VOLUMES_SOCKET_GID` GID of the truenas-docker-volumes socket

## Usage

Create volumes in Docker with the `--driver=truenas-docker-volumes` option. For example:
```
# docker volume create --driver=truenas-docker-volumes test
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GNU LGPLv3](https://choosealicense.com/licenses/lgpl-3.0/)
