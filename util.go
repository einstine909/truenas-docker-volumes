package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/dariusbakunas/truenas-go-sdk"
)

func GetDataset(api_client *truenas.APIClient, dataset_id string) (dataset truenas.Dataset, err error) {
	dataset, r, err := api_client.DatasetApi.GetDataset(context.Background(), dataset_id).Execute() // Ask TrueNAS about the dataset

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DatasetApi.GetDataset``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
		return truenas.Dataset{}, err
	}

	return dataset, err
}

func LogMountedBy(driver *VolumeDriver, volume_name string) {
	id_list := "["

	for i, id := range driver.mounts[volume_name] {
		id_list += id
		if i < len(driver.mounts[volume_name])-1 {
			id_list += ", "
		}
	}
	id_list += "]"

	log.Printf("Volume %s is now mounted by: %s", volume_name, id_list)
}
