package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"path/filepath"
	"sync"
	"syscall"

	truenas "github.com/dariusbakunas/truenas-go-sdk"
	"github.com/docker/go-plugins-helpers/volume"
	"github.com/moby/sys/mountinfo"
)

type VolumeDriver struct {
	api_client        *truenas.APIClient  // Init'd TrueNAS Client
	mount_point       string              // Plugin's Mountpoint for Volumes
	parent_dataset_id string              // Parent Dataset for Volume Datasets
	scope             string              // Plugin Scope
	mounts            map[string][]string // keep track of all of the mounts
	mutex             *sync.Mutex         // Keep it serial
}

func NewVolumeDriver(api_client *truenas.APIClient, parent_dataset_id string) VolumeDriver {
	volume_driver := VolumeDriver{
		scope:             "global", // Should always be global
		api_client:        api_client,
		mount_point:       volume.DefaultDockerRootDirectory,
		parent_dataset_id: parent_dataset_id,
		mounts:            make(map[string][]string),
		mutex:             &sync.Mutex{},
	}
	return volume_driver
}

func (volume_driver VolumeDriver) Capabilities() *volume.CapabilitiesResponse { // Docker asks this as the 'Handshake'
	log.Println("Got request to list capabilities")
	return &volume.CapabilitiesResponse{
		Capabilities: volume.Capability{
			Scope: volume_driver.scope,
		},
	}
}

func (volume_driver VolumeDriver) List() (*volume.ListResponse, error) { // Give Docker a list of Volumes
	log.Println("Got request to list volumes")
	var volume_list []*volume.Volume

	dataset, err := GetDataset(volume_driver.api_client, volume_driver.parent_dataset_id) // Ask TrueNAS about the parent dataset
	if err != nil {
		return &volume.ListResponse{Volumes: volume_list}, err
	}

	for _, child_map := range dataset.AdditionalProperties["children"].([]interface{}) { // It seems that the generated code does not handle the child datasets well... so now we have this one weird trick that compilers hate

		var child_dataset truenas.Dataset
		var truenas_volume volume.Volume

		child_json, marshal_err := json.Marshal(child_map) // That...
		if marshal_err != nil {
			fmt.Fprintln(os.Stderr, marshal_err)
		}

		marshal_err = child_dataset.UnmarshalJSON(child_json) // ...trick
		if marshal_err != nil {
			fmt.Fprintln(os.Stderr, marshal_err)
		}

		truenas_volume.Name = filepath.Base(child_dataset.Name) // Volume name is everything after the last '/'

		mountpoint := volume_driver.mount_point + "/" + truenas_volume.Name

		mounted, err := mountinfo.Mounted(mountpoint) // Search the mount table for our mountpoint
		if err == nil {
			if !mounted {
				truenas_volume.Mountpoint = mountpoint // Set the mountpoint only if its actualy mounted
			}
		}

		volume_list = append(volume_list, &truenas_volume)
	}

	return &volume.ListResponse{Volumes: volume_list}, err
}

func (volume_driver VolumeDriver) Create(create_volume_req *volume.CreateRequest) error { // Create a dataset on TrueNAS and return it as a Volume to Docker
	log.Printf("Got request to create volume %s\n", create_volume_req.Name)
	volume_driver.mutex.Lock() // We gotta pay attention

	params := *truenas.NewCreateDatasetParams(volume_driver.parent_dataset_id + "/" + create_volume_req.Name)

	new_dataset, http_res, err := volume_driver.api_client.DatasetApi.CreateDataset(context.Background()).CreateDatasetParams(params).Execute()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DatasetApi.CreateDataset``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", http_res)
		return err
	}

	var nfs_paths []string
	nfs_paths = append(nfs_paths, *new_dataset.Mountpoint)
	var nfs_params = truenas.NewCreateShareNFSParams(nfs_paths)

	nfs_params.SetMapallUser("root")
	nfs_params.SetMapallGroup("wheel")
	nfs_params.SetQuiet(true)
	nfs_params.SetEnabled(true)

	_, http_res, err = volume_driver.api_client.SharingApi.CreateShareNFS(context.Background()).CreateShareNFSParams(*nfs_params).Execute() // Blame OpenAPI Go code generation for the anonymous function madness
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `SharingApi.CreateShareNFS``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", http_res)
		return err
	}

	volume_driver.mutex.Unlock() // We can talk to others again
	return err
}

func (volume_driver VolumeDriver) Get(get_req *volume.GetRequest) (*volume.GetResponse, error) { // Return info about a volume
	log.Printf("Got request to get volume %s\n", get_req.Name)
	mountpoint := volume_driver.mount_point + "/" + get_req.Name

	_, err := GetDataset(volume_driver.api_client, volume_driver.parent_dataset_id+"/"+get_req.Name)
	if err != nil {
		log.Println(err)
		return &volume.GetResponse{}, err
	}

	vol := &volume.Volume{Name: get_req.Name}

	mounted, _ := mountinfo.Mounted(mountpoint) // Search the mount table for our mountpoint
	if mounted {
		vol.Mountpoint = mountpoint // Set the mountpoint only if its actualy mounted
	}

	return &volume.GetResponse{Volume: vol}, err
}

func (volume_driver VolumeDriver) Path(path_req *volume.PathRequest) (*volume.PathResponse, error) {
	log.Printf("Got request for path of volume %s\n", path_req.Name)
	mountpoint := volume_driver.mount_point + "/" + path_req.Name

	mounted, err := mountinfo.Mounted(mountpoint) // Search the mount table for our mountpoint
	if mounted {
		return &volume.PathResponse{Mountpoint: mountpoint}, err
	}

	return &volume.PathResponse{}, err
}

func (volume_driver VolumeDriver) Mount(mount_req *volume.MountRequest) (*volume.MountResponse, error) {
	log.Printf("Got request to mount volume %s for %s\n", mount_req.Name, mount_req.ID)
	volume_driver.mutex.Lock() // We gotta pay attention

	dataset, err := GetDataset(volume_driver.api_client, volume_driver.parent_dataset_id+"/"+mount_req.Name)
	if err != nil {
		volume_driver.mutex.Unlock() // We can talk to others again
		return &volume.MountResponse{}, err
	}

	mountpoint := volume_driver.mount_point + "/" + mount_req.Name

	err = os.MkdirAll(mountpoint, 504)
	if err != nil {
		volume_driver.mutex.Unlock() // We can talk to others again
		return &volume.MountResponse{}, err
	}

	addr, err := net.LookupIP(volume_driver.api_client.GetConfig().Host)
	if err != nil {
		volume_driver.mutex.Unlock() // We can talk to others again
		return &volume.MountResponse{}, err
	}

	err = syscall.Mount(":"+*dataset.Mountpoint, mountpoint, "nfs4", 0, "addr="+addr[0].String())
	if err != nil {
		volume_driver.mutex.Unlock() // We can talk to others again
		return &volume.MountResponse{}, err
	}

	if _, ok := volume_driver.mounts[mount_req.Name]; ok {
		volume_driver.mounts[mount_req.Name] = append(volume_driver.mounts[mount_req.Name], mount_req.ID)
	} else {
		volume_driver.mounts[mount_req.Name] = []string{mount_req.ID}
	}

	volume_driver.mutex.Unlock() // We can talk to others again
	LogMountedBy(&volume_driver, mount_req.Name)
	return &volume.MountResponse{Mountpoint: mountpoint}, err
}

func (volume_driver VolumeDriver) Unmount(unmount_req *volume.UnmountRequest) error {
	log.Printf("Got request to unmount %s for %s\n", unmount_req.Name, unmount_req.ID)
	volume_driver.mutex.Lock() // We gotta pay attention

	for i, id := range volume_driver.mounts[unmount_req.Name] {
		if id == unmount_req.ID {
			volume_driver.mounts[unmount_req.Name] = append(volume_driver.mounts[unmount_req.Name][:i], volume_driver.mounts[unmount_req.Name][i+1:]...)
		}
	}

	err := error(nil)

	if len(volume_driver.mounts[unmount_req.Name]) == 0 {
		mountpoint := volume_driver.mount_point + "/" + unmount_req.Name

		log.Printf("No references to %s; unmounting\n", unmount_req.Name)

		err = syscall.Unmount(mountpoint, 0)
		if err != nil {
			log.Println(err)
		}

		os.Remove(mountpoint)

	} else {
		LogMountedBy(&volume_driver, unmount_req.Name)
	}

	volume_driver.mutex.Unlock() // We can talk to others again
	return err
}

func (volume_driver VolumeDriver) Remove(remove_req *volume.RemoveRequest) error {
	log.Printf("Got request to remove %s\n", remove_req.Name)
	return errors.New("not implemented")
}
